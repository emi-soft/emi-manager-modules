manager-modules
===============
manager-modules

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist cms/manager-modules "*"
```

or add

```
"cms/manager-modules": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \managermodules\AutoloadExample::widget(); ?>```