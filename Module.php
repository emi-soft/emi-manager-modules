<?php

namespace emi\managermodules;


/**
 * rbac module definition class
 */
class Module extends \engine\base\components\module\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'emi\managermodules\controllers';
    public $defaultRoute = 'default';
    /**
     * {@inheritdoc}
     */
    public function init()
    {

        parent::init();

    }

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {

        // TODO: Implement bootstrap() method.
    }
}
