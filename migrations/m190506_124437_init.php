<?php

use yii\db\Migration;

/**
 * Class m190506_124437_init
 */
class m190506_124437_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        //Опции для mysql
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%module}}', [
            'id' => $this->primaryKey(),
            'include' => $this->string(32)->notNull(),
            'type' => $this->string(32),
            'url' => $this->string(255)->notNull(),
            'path' => $this->string(255),
            'status' => $this->boolean()
        ], $tableOptions);
        $this->alterColumn('{{%module}}', 'id', $this->integer(11) . ' NOT NULL AUTO_INCREMENT');

        $this->createTable('{{%module_config}}', [
            'id' => $this->primaryKey(),
            'module_id' => $this->integer(),
            'value' => $this->text()
        ], $tableOptions);
        $this->alterColumn('{{%module_config}}', 'id', $this->integer(11) . ' NOT NULL AUTO_INCREMENT');

        $this->addForeignKey('module', '{{%module_config}}', 'module_id', '{{%module}}', 'id', 'CASCADE', 'CASCADE');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('module_config');
        $this->dropTable('module');

    }

}
